﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GazeManager : MonoBehaviour
{
    public string TagToInteract = string.Empty;
    GameObject FocusedGameObject = null;


    public GameObject GazeCursor = null;

    [Tooltip("Maximum gaze distance for calculating a hit.")]
    public float MaxGazeDistance = 5.0f;

    [Tooltip("Select the layers raycast should target.")]
    public LayerMask RaycastLayerMask = Physics.DefaultRaycastLayers;

    /// <summary>
    /// Physics.Raycast result is true if it hits a Hologram.
    /// </summary>
    public bool Hit { get; private set; }

    /// <summary>
    /// HitInfo property gives access
    /// to RaycastHit public members.
    /// </summary>
    public RaycastHit HitInfo { get; private set; }

    /// <summary>
    /// Position of the user's gaze.
    /// </summary>
    public Vector3 Position { get; private set; }

    /// <summary>
    /// RaycastHit Normal direction.
    /// </summary>
    public Vector3 Normal { get; private set; }
    
    private Vector3 gazeOrigin;
    private Vector3 gazeDirection;

	
	// Update is called once per frame
	void Update ()
    {
        GameObject NewFocusedGameObject = null;

        //Assign Camera's main transform position to gazeOrigin.
        gazeOrigin = Camera.main.transform.position;

        //Assign Camera's main transform forward to gazeDirection.
        gazeDirection = Camera.main.transform.forward;

        //Create a variable hitInfo of type RaycastHit.
        RaycastHit hitInfo;

        //Perform a Unity Physics Raycast.
        // Collect return value in public property Hit.
        // Pass in origin as gazeOrigin and direction as gazeDirection.
        // Collect the information in hitInfo.
        // Pass in MaxGazeDistance and RaycastLayerMask.
        Hit = Physics.Raycast(gazeOrigin,
                       gazeDirection,
                       out hitInfo,
                       MaxGazeDistance,
                       RaycastLayerMask);

        //Assign hitInfo variable to the HitInfo public property 
        // so other classes can access it.
        HitInfo = hitInfo;

        if (Hit)
        {
            //Assign property Position to be the hitInfo point.
            Position = hitInfo.point;
            //Assign property Normal to be the hitInfo normal.
            Normal = hitInfo.normal;

            if (hitInfo.collider != null)
            {
                // Assign the hitInfo's collider gameObject
                NewFocusedGameObject = hitInfo.collider.gameObject; 
            }
        }
        else
        {
            //Assign Position to be gazeOrigin plus MaxGazeDistance times gazeDirection.
            Position = gazeOrigin + (gazeDirection * MaxGazeDistance);
            //Assign Normal to be the user's gazeDirection.
            Normal = gazeDirection;
        }
        
        //position gaze cursor
        if (GazeCursor != null)
        {
            GazeCursor.transform.position = Position;
            GazeCursor.transform.forward = Normal;
        }

        //If there was a change in the focus
        if (NewFocusedGameObject != FocusedGameObject)
        {
            if (FocusedGameObject != null && FocusedGameObject.tag == TagToInteract)
                FocusedGameObject.SendMessage("GazeExited", SendMessageOptions.DontRequireReceiver);

            if (NewFocusedGameObject != null && NewFocusedGameObject.tag == TagToInteract)
                NewFocusedGameObject.SendMessage("GazeEntered", SendMessageOptions.DontRequireReceiver);

            FocusedGameObject = NewFocusedGameObject;
        }
    }
}
