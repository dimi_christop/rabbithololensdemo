﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dance : MonoBehaviour
{
    Animation anim = null;
    public GameObject psystem = null;
	// Use this for initialization
	void Start ()
    {
        anim = GetComponent<Animation>();

        if (anim != null)
            anim.Stop();
	}
	
	public void GazeEntered()
    {
        if (anim != null)
            anim.Play();
    }

    public void GazeExited()
    {
        if (anim != null)
            anim.Stop();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.gameObject.tag == "Ball")
        {
            Destroy(this.gameObject);

            if (psystem != null)
            {
                GameObject particle = Instantiate(psystem, collision.contacts[0].point, Quaternion.identity);
                Destroy(particle, 3);
            }
        }
    }
}
