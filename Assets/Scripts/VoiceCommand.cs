﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Windows.Speech;

public class VoiceCommand : MonoBehaviour
{
    public GazeManager gazeManager = null;

    public string shootCommand = "shoot ball";
    public CannonShooter shooter = null;

    public string placeCommand = "place";
    public GameObject rabbit = null;

    public string resetCommand = "reset world";

    KeywordRecognizer _keywordRecognizer;

	// Use this for initialization
	void Start ()
    {

        _keywordRecognizer = new KeywordRecognizer(new[] {shootCommand, placeCommand, resetCommand});
        _keywordRecognizer.OnPhraseRecognized += onKeywordEvent;
        _keywordRecognizer.Start();
    }

    private void OnDestroy()
    {
        _keywordRecognizer.Stop();
    }

    public void onKeywordEvent(PhraseRecognizedEventArgs args)
    {
        string cmd = args.text;

        if (cmd == resetCommand)
            SceneManager.LoadSceneAsync(0, LoadSceneMode.Single);
        else if (cmd == shootCommand && shooter != null)
            shooter.Shoot();
        else if (cmd == placeCommand && rabbit != null && gazeManager != null)
        {
            if (gazeManager.Hit)
            {
                //Create rabbit on hit position with up vector the hit normal
                GameObject rabbitInstance = Instantiate(rabbit, gazeManager.Position, Quaternion.identity);
                rabbitInstance.transform.up = gazeManager.Normal;

                //make rabbit face us but locked to his local y axis
                //Project to plane, 
                float distanceToPlane = Vector3.Dot(rabbitInstance.transform.up, Camera.main.transform.position - rabbitInstance.transform.position);
                //Find point on plane
                Vector3 pointOnPlane = Camera.main.transform.position - (rabbitInstance.transform.up * distanceToPlane);
                //face rabbit
                rabbitInstance.transform.LookAt(pointOnPlane, rabbitInstance.transform.up);

                //tweack beacuse rabbit model has localaxis point backward
                rabbitInstance.transform.localRotation *= Quaternion.AngleAxis(180, Vector3.up);
            }
        }

    }
}
