﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VR.WSA.Input;

public class CannonShooter : MonoBehaviour
{
    public float forceMagnitude = 300;
    GestureRecognizer _recongizer;
    public GameObject canonBall = null;

    // Use this for initialization
    void Start ()
    {
        _recongizer = new GestureRecognizer();
        _recongizer.TappedEvent += tapEvent;
        _recongizer.SetRecognizableGestures(GestureSettings.Tap);
        _recongizer.StartCapturingGestures();
    }

    private void OnDestroy()
    {
        _recongizer.StopCapturingGestures();
    }

    public void Shoot()
    {
        GameObject ball = Instantiate(canonBall);

        Rigidbody rbody = ball.GetComponent<Rigidbody>();
        rbody.mass = 0.25f;
        rbody.position = transform.position;
        rbody.AddForce(transform.forward * forceMagnitude);
    }

    void tapEvent(InteractionSourceKind source, int tapCount, Ray headRay)
    {
        Shoot();
    }
}
