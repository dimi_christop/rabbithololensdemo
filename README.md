This is the Demo project created for the "Programming the Hololens with unity" presentation i gave on the
3/2/2017 at Stone Soup Athens and on the 22/2/2017 at Microsoft Hellas Athens.

For more visit 
https://developer.microsoft.com/en-us/windows/holographic/academy
The Bunny model used came form here.
https://www.assetstore.unity3d.com/en/#!/content/22788

Tested on Unity 5.5, and Windows 10.

A video of the Demo working can be seen here
https://youtu.be/D8TS-yV_1ag

Have Fun
Dimi Christopoulos
http://vrbytes.eu